import socketIO_client
import twitter
import os

twitterApi = twitter.Api(consumer_key=os.environ['TWITTER_CONSUMER_KEY'],
                  consumer_secret=os.environ['TWITTER_CONSUMER_SECRET'],
                  access_token_key=os.environ['TWITTER_ACCES_TOKEN_KEY'],
                  access_token_secret=os.environ['TWITTER_ACCES_TOKEN_SECRET'])

def publish_on_twitter(change):
    title = (change['title'][:47] + '...') if len(change['title']) > 50 else change['title']
    url_title = change['title'].replace(' ', '_')
    status =  twitterApi.PostUpdate('Krouet eo bet ar pennad %s gant %s %s/wiki/%s #bzhg #wikipedia' % (title, change['user'],change['server_url'], url_title))

def publish_on_facebook(change):
    print 'Not implemented yet'

class WikiNamespace(socketIO_client.BaseNamespace):

    def on_change(self, change):
        if change['type'] == 'new' and change['namespace'] == 0:
            publish_on_twitter(change)

    def on_connect(self):
        self.emit('subscribe', 'br.wikipedia.org')
        #comment the above and uncomment following line to get more testing data
        #self.emit('subscribe', 'commons.wikimedia.org')

socketIO = socketIO_client.SocketIO('stream.wikimedia.org', 80)
socketIO.define(WikiNamespace, '/rc')

socketIO.wait()

